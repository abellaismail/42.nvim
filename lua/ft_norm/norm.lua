local header 	= require("ft_norm.components.42Header")
local preproc	= require("ft_norm.components.preproc")
local global	= require("ft_norm.components.globals")
local funcs		= require("ft_norm.components.functions")

local M = {}

---
--- This functions checks 42 header
--- and fix it if something is wrong
---
--- @param root table representing root node of tree
--- @return table
---
function M.norm42Header(root)
	local lines = header.check(root)
	if lines == nil then
		lines = header.fix()
	end
	return lines
end

---
--- @param root table representing root node of tree
--- @return table
---
function M.pre_proc_inc(root)
	return preproc.check_includes(root);
end

---
--- @param root table representing root node of tree
--- @return table
---
function M.global_vars(root)
	return global.check(root)
end

---
--- @param root table representing root node of tree
--- @return table
---
function M.functions(root)
	return funcs.read(root)
end

return M
