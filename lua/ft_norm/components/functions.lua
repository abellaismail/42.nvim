local Utils = require("ft_norm.utils")
local var_cmp = require("ft_norm.components.variables")
local M = {}


local function read_proto(node)
	local ret = ""

	local cur = Utils.getNodeByFields(node, "type")
	ret = ret .. Utils.getToken(cur)[1] .. "\t"

	cur = Utils.getNodeByFields(node, "declarator.declarator")
	ret = ret .. Utils.getToken(cur)[1] .. ""

	cur = Utils.getNodeByFields(node, "declarator.parameters")
	ret = ret .. var_cmp.read_parameters(cur)
	return ret
end

local function read_func(node)
	local curnode = node:child(0)
	local lines = {}

	table.insert(lines, read_proto(node))

	curnode = Utils.getNodeByFields(node, "body"):child(0)
	while curnode ~= nil do
		vim.list_extend(lines, Utils.getToken(curnode))
	 	curnode = curnode:next_sibling()
	end
	return lines;
end

function M.read(root)
	local lines = {}
	local curnode = root:child(0)

	while curnode ~= nil do
		if curnode:type() == "function_definition"
			then
			vim.list_extend(lines, read_func(curnode))
		end
	 	curnode = curnode:next_named_sibling()
	end

	table.insert(lines, "a")
	return lines
end

return M
