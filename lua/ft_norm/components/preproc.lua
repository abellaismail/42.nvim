local Utils = require("ft_norm.utils")
local M = {}

function M.check_includes(root)
	local lines = {}

	local query = vim.treesitter.parse_query('c', [[(preproc_include) @preproc]])
	for _, curnode in query:iter_captures(root, 0, root:start(), root:end_()) do
		vim.list_extend(lines, Utils.getlines(curnode));
	end
	table.insert(lines, "")

	return lines
end

return M
