local Utils = require("ft_norm.utils")

local M = {}

---
--- @param root table representing root node of tree
--- @return table
---
function M.check(root)
	local node = root:child(0)

	if node == nil then
		return
	end

	local lines = {}

	local i = 0
	while node ~= nil and  node:type() == 'comment' do
		vim.list_extend(lines, Utils.getlines(node))
		i = i + 1
	 	node = node:next_named_sibling()
	 end
	 return lines;
end

---
--- @param root table representing root node of tree
--- @return table
---
function M.fix(root)
	-- TODO: fix header
end
return M
