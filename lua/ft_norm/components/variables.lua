local ut = require("ft_norm.utils")
local M = {}

function M.read_parameters(node)
	local ret = ""
	for item in node:iter_children()
		do
			if (item:type() == "parameter_declaration") then
				ret = ret .. M.read(item)
			else
				ret = ret .. ut.getToken(item)[1] .. ""
				if item:type() == "," then
					ret = ret .. " "
				end
			end
	end
	return ret
end

function M.read(node)
	local tk
	local ret

	tk	= ut.getNodeByFields(node, "type")
	ret = string.gsub(ut.getToken(tk)[1], "%s+", " ") .. " "

	tk	= ut.getNodeByFields(node, "declarator")
	if tk:type() == "function_declarator" then
		local sub_tk = ut.getNodeByFields(tk, "declarator")
		ret = ret .. string.gsub(ut.getToken(sub_tk)[1], "%s+", "")

		sub_tk = ut.getNodeByFields(tk, "parameters")
		ret = ret .. M.read_parameters(sub_tk)

	else
		ret = ret .. string.gsub(ut.getToken(tk)[1], "%s+", "")
	end
	return ret
end

return M
