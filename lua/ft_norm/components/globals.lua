local Utils = require("ft_norm.utils")

local M = {}

function M.check(root)
	local lines = {}
	local curnode = root:child(0)

	while curnode ~= nil do
		if curnode:type() == "declaration"
			then
			vim.list_extend(lines, Utils.getlines(curnode));
		end
	 	curnode = curnode:next_named_sibling()
	end

	table.insert(lines, "")
	return lines
end

return M
