local M = {}
local norm	= require("ft_norm.norm")

--[[
norm: force a code structure:
42 HEADER
PRPROC INCLUDE
GLOBAL VARIABLES
FUNCTIONS
]]--

function	M.format()
	local ts = vim.treesitter
	local parser = ts.get_parser(0, 'c')
	local tstree = parser:parse()[1]
	local root = tstree:root()

	local lines = {}

	vim.list_extend(lines, norm.norm42Header(root))
	vim.list_extend(lines, norm.pre_proc_inc(root))
	vim.list_extend(lines, norm.global_vars(root))
	vim.list_extend(lines, norm.functions(root))

	for _, line in pairs(lines)
	do
		print(line)
	end
end

return M
